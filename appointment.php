<!DOCTYPE html>
<html>
<head>
<title>KLinik My Dentist</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Clinical Lab Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->

<link href='//fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<script src="js/responsiveslides.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top" id="mainNav">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"><img alt="MyDentist" width="80px" src="images/logobaru2.png"></a>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- top-nav -->
			<div id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a  href="index.html">Home</a></li>
					<li><a href="about.html">About</a></li>
					<!-- 	<li><a href="codes.html">Short Codes</a></li> -->
					<li><a href="treatments.html">Treatments</a></li>
					<li><a href="contact.html">Contact</a></li>
					<li><a href="appointment.php" class="active">Appointment</a></li>
					<li><a href="cabang.html">Cabang</a></li>
				</ul>	
				<div class="clearfix"> </div>	
			</div>
		</div>
	</nav>

<!-- banner -->
<div class="banner_w3ls page_head">
	<div class="about">
		<h1>Appointment</h1>
		<h3>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h3>
	</div>
</div>
<!-- //banner -->
<div class="map all_pad">
	<div class="container">
		<h3 class="title agile">Appointments</h3>
		<div class="contact-grids w3layouts">
			
				<form>
  <div class="form-group">
    <label for="exampleFormControlInput1">Id Rec</label>
    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="16789065">
  </div>
  <div class="form-group">
    <label for="exampleFormControlInput1">Id Pasien</label>
    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="16789065">
  </div>
   <div class="form-group">
    <label for="exampleFormControlInput1">Nama Pasien</label>
    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Siti Fatimah">
  </div>
   <div class="form-group">
    <label for="exampleFormControlTextarea1">Alamat Pasien</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="Jl Pondok China No.67 Kecamatan Depok" rows="3"></textarea>
  </div>
  <div class="form-group">
    <label for="exampleFormControlInput1">Telepon Pasien</label>
    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="+6289789765456">
  </div>
    <div class="form-group">
    <label for="exampleFormControlInput1">Email Pasien</label>
    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="siti.fatimah@gmail.com">
  </div>

    <div class="form-group">
    <label for="exampleFormControlSelect2">Pilihan Dokter Pertama</label>
    <select multiple class="form-control" id="exampleFormControlSelect2">
      <option>Dr. Rina</option>
      <option>Dr. Rini</option>
      <option>Dr. Rana</option>
      <option>Dr. Rono</option>
      <option>Dr. Runi</option>
    </select>
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect2">Pilihan Dokter Kedua</label>
    <select multiple class="form-control" id="exampleFormControlSelect2">
      <option>Dr. Rina</option>
      <option>Dr. Rini</option>
      <option>Dr. Rana</option>
      <option>Dr. Rono</option>
      <option>Dr. Runi</option>
    </select>
  </div>
  
  <div class="form-group">
    <label for="exampleFormControlSelect1">Status Dokter Pertama</label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>Available</option>
      <option>Non Available</option>
    </select>
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Status Dokter Kedua</label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>Available</option>
      <option>Non Available</option>
    </select>
  </div>
  
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Keluhan Awal</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>
  
   <button type="submit" class="btn btn-primary">Submit</button>
 
  
</form>
				<div class="clearfix"> </div>
		</div>
		
	
	</div>
</div>
<!-- contact -->
<div class="contact_w3agile">
	<div class="container">
		<h2 class="title agileits-w3layouts" style="color: #fff;">Get In Touch</h2>
		<div class="strip"></div>
		<ul>
			<li><a class="fb-icon1" href="#"></a></li>
			<li><a class="fb-icon2" href="#"></a></li>
			<li><a class="fb-icon3" href="#"></a></li>
			<li><a class="fb-icon4" href="#"></a></li>
			<li><a class="fb-icon5" href="#"></a></li>
		</ul>
		<form action="#" method="post">
			<input type="text" value="Name" name="Name" onfocus="this.value='';" onblur="if (this.value == '') {this.value ='Name';}">
			<input type="email" value="Email" name="Email" onfocus="this.value='';" onblur="if (this.value == '') {this.value ='Email';}">
			<textarea name="Message" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}" required="">Message</textarea>
			<div class="con-form text-center">
				<input type="submit" value="Send">
			</div>
		</form>
		<!-- <p class="agileinfo">&copy; 2017 Clinical Lab . All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p> -->
	</div>
</div>
<!-- smooth scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
		/*
			var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
			};
		*/								
		$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
<script src="js/kmd.js"></script>

<!-- datepicker -->


</body>
</html>